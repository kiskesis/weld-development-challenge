import React, { useEffect } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types';
import './NameChanger.css';
import { connect } from 'react-redux';
import { getUsersAction } from '../../actions/UsersActions';
import { deleteUserRequest } from '../../services/UsersAPI';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

function NameChanger(props) {

    useEffect(() => {
        const fetchData = async () => {
            const { getUsers } = props;
            await getUsers();
        };

        fetchData();
    }, []);

    const deleteUser = async (id) => {
        await deleteUserRequest(id)
        alert('User Deleted');
    }

    return (
        <Container className="nameChanger">
            <Row>
                <Col xs={12} md={6}>
                    Name
                </Col>
            </Row>
            <Row className="nameChangerList">
                <Col xs={12} md={6}>
                    {
                        props.users.map(({name, id}) => (
                            <div key={id} className="nameChangerRow">
                                <div className="nameChangerNameDiv">
                                    <p>{ name }</p>
                                </div>
                                <Link to={`/namechanger/change/${id}`}>
                                    <Button color="info">Change</Button>
                                </Link>
                                <FontAwesomeIcon
                                    icon="trash"
                                    className="nameChangerDeleteUserButton"
                                    onClick={() => deleteUser(id)}
                                />
                            </div>
                        ))
                    }
                </Col>
            </Row>
        </Container>
    )
}

NameChanger.propTypes = {
    getUsers: PropTypes.func,
}

const mapDispatchToProps = dispatch => ({
    getUsers: async () => dispatch(await getUsersAction()),
});

const mapStateToProps = state => ({
    ...state.usersReducer,
})

export default connect(mapStateToProps, mapDispatchToProps)(NameChanger)