import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Button, FormGroup, Form, Input } from 'reactstrap';
import PropTypes from 'prop-types';
import './Changer.css';
import { connect } from 'react-redux';
import { saveUserAction, getUserAction } from '../../actions/UsersActions';

function Changer(props) {
    const [name, setName] = useState('');

    useEffect(() => {
        const fetchData = async () => {
            const { getUser, match } = props;
            const { id } = match.params;
            await getUser(id);
        };

        fetchData();
    }, []);

    const saveUser = async () => {
        const { saveUser, user, history } = props;
        const { id } = user;

        await saveUser(id, name)
        alert('User Saved');
        history.push('/namechanger')
    }

    return (
        <Container className="changer">
            <Row>
                <Col xs={12} md={{ offset: 1, size: 10 }}>
                    <h2>Name</h2>
                </Col>
            </Row>
            <Row className="changerRow">
                <Col md={{ offset: 1, size: 10 }}>
                    <Form>
                        <FormGroup row>
                            <Col sm={12} md={4}>
                                <Input
                                    type="email"
                                    name="email"
                                    id="exampleEmail"
                                    placeholder={props.user && props.user.name}
                                    onChange={(e) => setName(e.target.value)}
                                />
                            </Col>
                            <Col sm={2}>
                                <Button
                                    onClick={saveUser}
                                    color="danger"
                                >
                                    Save
                                </Button>
                            </Col>
                        </FormGroup>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}

Changer.propTypes = {
    getUser: PropTypes.func,
    saveUser: PropTypes.func,
}

const mapDispatchToProps = dispatch => ({
    saveUser: async (id, name) => dispatch(await saveUserAction(id, name)),
    getUser: async (id) => dispatch(await getUserAction(id)),
});

const mapStateToProps = state => ({
    ...state.usersReducer,
})

export default connect(mapStateToProps, mapDispatchToProps)(Changer)