export const actionTypes = {
    GET_USERS: 'GET_USERS',
    GET_USER: 'GET_USER',
    UPDATE_USER_NAME: 'UPDATE_USER_NAME',
}