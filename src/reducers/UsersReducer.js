import { actionTypes } from '../constants/ActionTypes';

const initialState = {
  users: [],
  user: null,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.GET_USERS:
      return {
        ...state,
        users: action.payload.users,
      };
    case actionTypes.GET_USER:
      return {
        ...state,
        user: action.payload.user,
      };
    case actionTypes.UPDATE_USER_NAME:
      return {
        ...state,
        user: action.payload.user,
      };
    default:
      return state;
  }
}
