import basicRequest from "./basicRequest";

export const getUsersRequest = async () => {
    return basicRequest.get('/users')
}

export const getUserRequest = async (id) => {
    return basicRequest.get(`/users/${id}`)
}

export const deleteUserRequest = async (id) => {
    return basicRequest.delete(`/users/${id}`)
}

export const saveUserRequest = async (id, name) => {
    return basicRequest.patch(`/users/${id}`, {
        name,
    })
}