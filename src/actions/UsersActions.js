import { getUsersRequest, getUserRequest, saveUserRequest } from '../services/UsersAPI';
import { actionTypes } from '../constants/ActionTypes';

export const getUsersAction = async () => {
  const { data } = await getUsersRequest();

  return {
    type: actionTypes.GET_USERS,
    payload: {
      users: data
    },
  };
}

export const getUserAction = async (id) => {
  const { data } = await getUserRequest(id);

  return {
    type: actionTypes.GET_USER,
    payload: {
      user: data
    },
  };
}

export const saveUserAction = async (id, name) => {
  const { data } = await saveUserRequest(id, name);

  return {
    type: actionTypes.UPDATE_USER_NAME,
    payload: {
      user: data
    },
  };
}
