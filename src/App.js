import React from 'react';
import './App.css';
import { Provider } from 'react-redux';
import Router from './router';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import reducer from './reducers/index';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faTrash } from '@fortawesome/free-solid-svg-icons'

library.add(faTrash)

const store = createStore(reducer, applyMiddleware(thunk));

function App() {
  return (
      <Provider store={store}>
        <Router />
      </Provider>
  );
}

export default App;
