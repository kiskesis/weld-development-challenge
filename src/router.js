import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import NameChanger from './components/NameChanger/NameChanger.js';
import Changer from './components/Changer/Changer.js';

export default () => (
  <Router>
      <Route exact path="/namechanger" component={NameChanger}/>
      <Route path="/namechanger/change/:id" component={Changer}/>
  </Router>
);
